## Installation
```bash
npm install --save @centrepointmedia/tt-xml-to-json 
```

## API
```javascript
DvsaContentConverter.convert(data: string | string[]): object
```

## Example Usage
### Read and write a single file
```javascript
import fs from 'fs';
import {DvsaContentConverter} from "@centrepointmedia/tt-xml-to-json";

fs.readFile('./AB2003.xml', "UTF-8", (err, data) => {
    if(err) {
        console.error(err)
    }

    DvsaContentConverter.convert(data)
        .then(either => either.matchWith({
            left: console.error,
            right: json => fs.writeFile('./AB2003.json', JSON.stringify(json), (err) => {
                if(err) {
                    return console.error(err);
                }

                console.log("Done");
            })
        }));

});
```
### Read a directory of files and write the JSON to a directory
```javascript
import fs from 'fs';
import {DvsaContentConverter} from "@centrepointmedia/tt-xml-to-json";

const readFile = (file) => new Promise((resolve, reject) => 
    fs.readFile(file, "UTF-8", (err, data) => {
        if(err) {
            return reject(err);
        }

        return resolve(data);
    }));

const writeFile = ({name, data}) => new Promise((resolve, reject) => 
    fs.writeFile(name, data, (err) => {
        if(err) {
            return reject(err);
        }

        resolve();
    }));

fs.readdir('./data/xml', (err, files) => {
    if(err) {
        console.log(err);
        return;
    }
    
    Promise.all(files.map(file => `./data/xml/${file}`).map(readFile))
        .then(DvsaContentConverter.convert)
        .then(either => either.matchWith({left: console.error, right: v => 
            Promise.all(
                v.map(json => ({name: `./data/json/${json.identifier}.json`, data: JSON.stringify(json)}))
                .map(writeFile)
            )}));
});
```
