import xml2js from "xml2js";
import {TryAsync} from "@centrepointmedia/functional-js";

const dvsaContentConverterFactory = function() {
    const parser = new xml2js.Parser();
    const parseData = (data) => parser.parseStringPromise(data)
        .then((res) => {
            // exempt response return "no" or "yes"
            const exemptFunction = function () {
                if (res.QF.$.ni_exempt === "yes") {
                    return true;
                } else {
                    return false;
                }
            }
            
            return ({
                "prompt": res.QF.question[0].prompt[0],
                "identifier": res.QF.$.id,
                "niExempt": exemptFunction(),
                "graphic": res.QF.question[0].graphic[0],
                "explanation": {
                    "text": res.QF.explanation[0].text[0]
                },

                "answers": [
                    {
                        "correct": res.QF.answers[0].answer[0].$.correct,
                        "text": res.QF.answers[0].answer[0].text[0],
                        "graphic": res.QF.answers[0].answer[0].graphic[0]
                    },
                    {
                        "correct": res.QF.answers[0].answer[1].$.correct,
                        "text": res.QF.answers[0].answer[1].text[0],
                        "graphic": res.QF.answers[0].answer[1].graphic[0]
                    },
                    {
                        "correct": res.QF.answers[0].answer[2].$.correct,
                        "text": res.QF.answers[0].answer[2].text[0],
                        "graphic": res.QF.answers[0].answer[2].graphic[0]
                    },
                    {
                        "correct": res.QF.answers[0].answer[3].$.correct,
                        "text": res.QF.answers[0].answer[3].text[0],
                        "graphic": res.QF.answers[0].answer[3].graphic[0]
                    }
                ],
                "xref": res.QF.question[0].xref[0],
                "text": res.QF.question[0].text[0]
            });
        });

    const convert = (data) => 
        TryAsync(() => Array.isArray(data) ? Promise.all(data.map(parseData)) : parseData(data));
    
    return {
        convert
    };
}

const DvsaContentConverter = dvsaContentConverterFactory();

export {DvsaContentConverter};